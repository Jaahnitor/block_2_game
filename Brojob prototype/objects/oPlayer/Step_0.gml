//Key presses
shift_key = keyboard_check_pressed(vk_shift);
right_key = keyboard_check(ord("D")) or keyboard_check(vk_right);
up_key = keyboard_check(ord("W")) or keyboard_check(vk_up);
down_key = keyboard_check(ord("S")) or keyboard_check(vk_down);
left_key = keyboard_check(ord("A")) or keyboard_check(vk_left);

//get xspd and yspd

xspd = (right_key - left_key) * move_spd;
yspd = (down_key - up_key) * move_spd;

//Pauser

if instance_exists(oPauser)
	{
	xspd = 0;
	yspd = 0;
	}

//set sprite

mask_index = sprite[DOWN]
if yspd == 0
	{
if xspd > 0 {face = RIGHT};
if xspd < 0 {face = LEFT};
	}
if xspd > 0 && face == LEFT {face = RIGHT};
if xspd < 0 && face == RIGHT {face = LEFT};
if xspd == 0
	{
if yspd > 0 {face = DOWN};
if yspd < 0 {face = UP};
	}
if yspd > 0 && face == UP {face = DOWN};
if yspd < 0 && face == DOWN {face = UP};

sprite_index = sprite[face];


//collisions

if place_meeting(x + xspd, y, oWall)
	{
	xspd = 0;	
	}
if place_meeting(x, y + yspd, oWall)
	{
	yspd = 0;	
	}

//move player

x += xspd;
y += yspd;

if shift_key { move_spd =+ fast_spd }

//Animate

if xspd == 0 && yspd == 0
	{
	image_index = 0;	
	}
	
//depth

depth = -bbox_bottom;