depth = -9980;

//item contrustor
function create_item(_name, _desc, _spr, _effect) constructor
	{
	name = _name;
	description = _desc;
	sprite = _spr;
	effect = _effect;
	}



//create the items

global.item_list = 
	{
	scissors : new create_item(
			"Scissors",
			"The elusive pair",
			sScissors,
			
			function()
			{
			
			}
		),
	cardboard : new create_item(
			"Pile of carboard",
			"Just a pile of cardboard",
			sCardboard,
		
			function()
				{
				
				}
		),
	markers : new create_item(
			"A box of Markers",
			"It's got all the colors of the rainbow and some more",
			sMarkers,
		
				function()
				{
			
				}
		),
	phone : new create_item(
			"Lost phone",
			"I wonder whose it is",
			sPhone,
		
			function()
				{
				
				}
		),
	Food : new create_item(
			"Breakfast",
			"A good way to start your day",
			sFood,
		
			function()
				{
				oPlayer.hp += 10;
			
				//get rid of object
				array_delete(inv, selected_item, 1);
				}
		
		),
	}
	

//create inventory
inv = array_create(0);

selected_item = -1;
		
//for drawing and mouse positions
sep = 16;
screen_bord = 16;