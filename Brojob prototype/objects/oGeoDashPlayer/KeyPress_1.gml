/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 4F6C06D7
/// @DnDArgument : "var" "T"
/// @DnDArgument : "op" "1"
if(T < 0)
{
	/// @DnDAction : YoYo Games.Collisions.If_Object_At
	/// @DnDVersion : 1.1
	/// @DnDHash : 63AD4A36
	/// @DnDParent : 4F6C06D7
	/// @DnDArgument : "x_relative" "1"
	/// @DnDArgument : "y" "1"
	/// @DnDArgument : "y_relative" "1"
	/// @DnDArgument : "object" "owallgeo"
	/// @DnDSaveInfo : "object" "owallgeo"
	var l63AD4A36_0 = instance_place(x + 0, y + 1, owallgeo);
	if ((l63AD4A36_0 > 0))
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 6CB9E5C8
		/// @DnDParent : 63AD4A36
		/// @DnDArgument : "var" "T"
		T = 0;
	}
}

/// @DnDAction : YoYo Games.Collisions.If_Object_At
/// @DnDVersion : 1.1
/// @DnDHash : 30FA053A
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y" "1"
/// @DnDArgument : "y_relative" "1"
/// @DnDArgument : "object" "owallgeo"
/// @DnDArgument : "not" "1"
/// @DnDSaveInfo : "object" "owallgeo"
var l30FA053A_0 = instance_place(x + 0, y + 1, owallgeo);
if (!(l30FA053A_0 > 0))
{
	/// @DnDAction : YoYo Games.Instances.Sprite_Rotate
	/// @DnDVersion : 1
	/// @DnDHash : 01344DCE
	/// @DnDParent : 30FA053A
	/// @DnDArgument : "angle" "90"
	/// @DnDArgument : "angle_relative" "1"
	image_angle += 90;
}