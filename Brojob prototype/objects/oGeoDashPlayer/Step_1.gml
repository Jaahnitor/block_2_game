/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 02301A9E
/// @DnDArgument : "var" "T"
/// @DnDArgument : "op" "1"
if(T < 0)
{
	/// @DnDAction : YoYo Games.Collisions.If_Object_At
	/// @DnDVersion : 1.1
	/// @DnDHash : 6207DBF2
	/// @DnDParent : 02301A9E
	/// @DnDArgument : "x_relative" "1"
	/// @DnDArgument : "y" "1"
	/// @DnDArgument : "y_relative" "1"
	/// @DnDArgument : "object" "owallgeo"
	/// @DnDArgument : "not" "1"
	/// @DnDSaveInfo : "object" "owallgeo"
	var l6207DBF2_0 = instance_place(x + 0, y + 1, owallgeo);
	if (!(l6207DBF2_0 > 0))
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 1468BDC6
		/// @DnDParent : 6207DBF2
		/// @DnDArgument : "expr" "V0 / A"
		/// @DnDArgument : "var" "T"
		T = V0 / A;
	}
}