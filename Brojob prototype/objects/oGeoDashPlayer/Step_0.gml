/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 70650C44
/// @DnDArgument : "expr" "6"
/// @DnDArgument : "expr_relative" "1"
/// @DnDArgument : "var" "direction"
direction += 6;

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 06F46C7F
/// @DnDArgument : "var" "T"
/// @DnDArgument : "op" "4"
if(T >= 0)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 08393316
	/// @DnDParent : 06F46C7F
	/// @DnDArgument : "expr" "V0 - (A * T)"
	/// @DnDArgument : "var" "VT"
	VT = V0 - (A * T);

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 7DA2AE62
	/// @DnDParent : 06F46C7F
	/// @DnDArgument : "expr" "1"
	/// @DnDArgument : "expr_relative" "1"
	/// @DnDArgument : "var" "T"
	T += 1;

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 508C859A
	/// @DnDParent : 06F46C7F
	/// @DnDArgument : "var" "VT"
	/// @DnDArgument : "op" "2"
	if(VT > 0)
	{
		/// @DnDAction : YoYo Games.Common.Execute_Code
		/// @DnDVersion : 1
		/// @DnDHash : 6158AA28
		/// @DnDParent : 508C859A
		/// @DnDArgument : "code" "move_contact_solid(90, VT);"
		move_contact_solid(90, VT);
	}

	/// @DnDAction : YoYo Games.Common.Else
	/// @DnDVersion : 1
	/// @DnDHash : 6A95241B
	/// @DnDParent : 06F46C7F
	else
	{
		/// @DnDAction : YoYo Games.Common.Execute_Code
		/// @DnDVersion : 1
		/// @DnDHash : 5B0E9156
		/// @DnDParent : 6A95241B
		/// @DnDArgument : "code" "move_contact_solid(270, -VT + 0.0001);"
		move_contact_solid(270, -VT + 0.0001);
	}
}