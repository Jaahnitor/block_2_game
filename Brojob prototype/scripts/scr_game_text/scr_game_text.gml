// Index for ALL DIALOGUE
/// @param text_id
function scr_game_text(_text_id){
	
switch(_text_id){
	
	case "npc 1":
			scr_text("uhh...");	
			scr_text("Hello!");	
			scr_text("I'm Timothy the Tutorial Demon!");	
			scr_text("Nice to finally meet you.");
			scr_text("Do you understand me?");
				scr_options("Yeah", "npc 1 - yes");
				scr_options("Huh? Why are you in my house?", "npc 1 - no");
		break;
		case "npc 1 - yes":
			scr_text("Oh good! You figured out by now that 'E' to talk to people.");
			scr_text("Use the 'WASD KEYS' or the 'ARROW KEYS' to run around.");
			scr_text("Feel free to use whatever your comfortable with.");
			scr_text("Anyway are you excited for your first day at Hanze University?");
				scr_options("Heck yeah!", "npc 1 - yeah!");
				scr_options("Not really...", "npc 1 - not really");
			break;
		case "npc 1 - yeah!":
			scr_text("That's the spirit!");
			scr_text("You're gonna do great kiddo. Now go out there and start your day!");
			break;
		case "npc 1 - not really":
			scr_text("ahh you seem a little nervous.");
			scr_text("You've probably heard this alot but I want you to hear it from me too.");
			scr_text("I can't contain my feelings properly cause I'm a demon and all so I'm gonna yell it.");
			scr_text("I'M SUPER PROUD OF YOU!!!!");
			scr_text("You made it this far and I know that you're gonna have a blast so go out there and have fun!");
			break;
		case "npc 1 - no":
			scr_text("Well then, if you're gonna act like this I wont help!");
			scr_text("Why don't you just go to the campus instead of being mean to me.");
			break;	
		
		//---------------------------------------------------------------------//
		
		
	case "npc 2":
			scr_text("Hello!");
			scr_text("I'm also Timothy the Tutorial Demon!");
			scr_text("The other guy is a fake, I can actually give you a tip.");
			scr_text("Press 'SHIFT' to run from your problems.");
			scr_text("But there is a catch!");
			scr_text("Once you start running, you never stop running...");
			scr_text("...");
			scr_text("Also you can use the 'MOUSE' to use the contents in your pocket.");
			scr_text("try it out on your breakfast I kindly left on the floor");
			scr_text("I'll keep making you breakfast when you're not home so feel free to come back if you're hungry.");
		break;
		
		
		//-------------------------------------------------------------------------//
		
		
	case "npc 3":
			scr_text("Welcome to your introduction day!");
			scr_text(" My name is Oliebol and I will be showing you around on your first day.");
			scr_text("Well I would show you around if I could but someone superglued my pants to this wall...");
			scr_text("Anyway");
			scr_text("Today you and your group will be making a simple board game, but before we can start we need some materials.");
			scr_text("Since you’re the last to arrive it’s only fair you get them, don’t you think?");
			scr_text("You’ll need a pair of scissors, some cardboard and the box of markers.");
				scr_options("Alright, where would I find the storage room", "npc 3 - where");
				scr_options("Fair. Right to it then", "npc 3 - yes");	
		break;
		
		case "npc 3 - where":
			scr_text("Head straight down and then turn right. Keep going till you find a classroom with your friends and at the back of that classroom is the storage room.")
			scr_text("You'll find it, good luck!");
			break;
			
		case "npc 3 - yes":
			scr_text("Woah not so fast, do you know where the storage room is?");
				scr_options("Yup","npc 3 - yup");
				scr_options("No","npc 3 - where");
			break;
		case "npc 3 - yup":
			scr_text("Alright then, Good luck!")
			break;
		
		//-------------------------------------------------------------------------//
		
		
	case "npc 4":
			scr_text("Hi!");		
		break;
		
		
		//-------------------------------------------------------------------------//
		
		
	case "npc 5":
			scr_text("Hello");		
		break;
		
		
		//-------------------------------------------------------------------------//
		
		
	case "npc 6":
			scr_text("Hey");		
		break;

}

}