{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 15,
  "bbox_right": 128,
  "bbox_top": 120,
  "bbox_bottom": 191,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 144,
  "height": 192,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"3398e43b-34a7-4848-8809-65051fc37fdb","path":"sprites/sHomes/sHomes.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3398e43b-34a7-4848-8809-65051fc37fdb","path":"sprites/sHomes/sHomes.yy",},"LayerId":{"name":"c3c78f47-e5bb-45b1-b198-1b17f77a57bd","path":"sprites/sHomes/sHomes.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sHomes","path":"sprites/sHomes/sHomes.yy",},"resourceVersion":"1.0","name":"3398e43b-34a7-4848-8809-65051fc37fdb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"67f7b24e-c1ef-4c60-9d05-dac026be22b8","path":"sprites/sHomes/sHomes.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"67f7b24e-c1ef-4c60-9d05-dac026be22b8","path":"sprites/sHomes/sHomes.yy",},"LayerId":{"name":"c3c78f47-e5bb-45b1-b198-1b17f77a57bd","path":"sprites/sHomes/sHomes.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sHomes","path":"sprites/sHomes/sHomes.yy",},"resourceVersion":"1.0","name":"67f7b24e-c1ef-4c60-9d05-dac026be22b8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f53e264c-8d70-439f-b49b-d5464b8ae141","path":"sprites/sHomes/sHomes.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f53e264c-8d70-439f-b49b-d5464b8ae141","path":"sprites/sHomes/sHomes.yy",},"LayerId":{"name":"c3c78f47-e5bb-45b1-b198-1b17f77a57bd","path":"sprites/sHomes/sHomes.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sHomes","path":"sprites/sHomes/sHomes.yy",},"resourceVersion":"1.0","name":"f53e264c-8d70-439f-b49b-d5464b8ae141","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sHomes","path":"sprites/sHomes/sHomes.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 0.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 3.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"5bbb7ae8-e0af-4e07-b322-3e19b1064ce6","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3398e43b-34a7-4848-8809-65051fc37fdb","path":"sprites/sHomes/sHomes.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2bf3dfb4-aec1-4b0f-a524-a898fcc97a65","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"67f7b24e-c1ef-4c60-9d05-dac026be22b8","path":"sprites/sHomes/sHomes.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"aeb14705-11ac-4fe7-9d12-ebdafc4df488","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f53e264c-8d70-439f-b49b-d5464b8ae141","path":"sprites/sHomes/sHomes.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sHomes","path":"sprites/sHomes/sHomes.yy",},
    "resourceVersion": "1.3",
    "name": "sHomes",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"c3c78f47-e5bb-45b1-b198-1b17f77a57bd","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "sHomes",
  "tags": [],
  "resourceType": "GMSprite",
}