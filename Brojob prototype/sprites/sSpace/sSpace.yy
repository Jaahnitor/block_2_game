{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 59,
  "bbox_top": 16,
  "bbox_bottom": 37,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 60,
  "height": 60,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"25fec092-8b8f-46f3-b484-6f22b3560597","path":"sprites/sSpace/sSpace.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"25fec092-8b8f-46f3-b484-6f22b3560597","path":"sprites/sSpace/sSpace.yy",},"LayerId":{"name":"63678e3d-6fd8-426e-aa5b-82de766254e4","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sSpace","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"25fec092-8b8f-46f3-b484-6f22b3560597","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"69d82002-1185-4ece-9c02-27f8bc45cd0b","path":"sprites/sSpace/sSpace.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"69d82002-1185-4ece-9c02-27f8bc45cd0b","path":"sprites/sSpace/sSpace.yy",},"LayerId":{"name":"63678e3d-6fd8-426e-aa5b-82de766254e4","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sSpace","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"69d82002-1185-4ece-9c02-27f8bc45cd0b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cb886bea-7c4a-4c15-a6d3-cf720a43f3d8","path":"sprites/sSpace/sSpace.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cb886bea-7c4a-4c15-a6d3-cf720a43f3d8","path":"sprites/sSpace/sSpace.yy",},"LayerId":{"name":"63678e3d-6fd8-426e-aa5b-82de766254e4","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sSpace","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"cb886bea-7c4a-4c15-a6d3-cf720a43f3d8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"49ac27d7-1c56-4e64-9d96-132a7148beaa","path":"sprites/sSpace/sSpace.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"49ac27d7-1c56-4e64-9d96-132a7148beaa","path":"sprites/sSpace/sSpace.yy",},"LayerId":{"name":"63678e3d-6fd8-426e-aa5b-82de766254e4","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sSpace","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"49ac27d7-1c56-4e64-9d96-132a7148beaa","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"86b9a167-e61d-4437-8651-d6ceab5de76f","path":"sprites/sSpace/sSpace.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"86b9a167-e61d-4437-8651-d6ceab5de76f","path":"sprites/sSpace/sSpace.yy",},"LayerId":{"name":"63678e3d-6fd8-426e-aa5b-82de766254e4","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sSpace","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"86b9a167-e61d-4437-8651-d6ceab5de76f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"aa90f3d3-871f-4499-bd93-c0077bfe39d0","path":"sprites/sSpace/sSpace.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"aa90f3d3-871f-4499-bd93-c0077bfe39d0","path":"sprites/sSpace/sSpace.yy",},"LayerId":{"name":"63678e3d-6fd8-426e-aa5b-82de766254e4","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sSpace","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"aa90f3d3-871f-4499-bd93-c0077bfe39d0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5f85bfb2-e15c-4003-83c1-4be01e304d92","path":"sprites/sSpace/sSpace.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5f85bfb2-e15c-4003-83c1-4be01e304d92","path":"sprites/sSpace/sSpace.yy",},"LayerId":{"name":"63678e3d-6fd8-426e-aa5b-82de766254e4","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sSpace","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"5f85bfb2-e15c-4003-83c1-4be01e304d92","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fc132e80-2bec-4df4-9637-a53b65f66d37","path":"sprites/sSpace/sSpace.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fc132e80-2bec-4df4-9637-a53b65f66d37","path":"sprites/sSpace/sSpace.yy",},"LayerId":{"name":"63678e3d-6fd8-426e-aa5b-82de766254e4","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sSpace","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"fc132e80-2bec-4df4-9637-a53b65f66d37","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"470bd843-ca4c-4f06-8979-c9e397763328","path":"sprites/sSpace/sSpace.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"470bd843-ca4c-4f06-8979-c9e397763328","path":"sprites/sSpace/sSpace.yy",},"LayerId":{"name":"63678e3d-6fd8-426e-aa5b-82de766254e4","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sSpace","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"470bd843-ca4c-4f06-8979-c9e397763328","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bc4c7caa-e2b7-4ba3-8849-a0dd4e4067a0","path":"sprites/sSpace/sSpace.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bc4c7caa-e2b7-4ba3-8849-a0dd4e4067a0","path":"sprites/sSpace/sSpace.yy",},"LayerId":{"name":"63678e3d-6fd8-426e-aa5b-82de766254e4","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sSpace","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"bc4c7caa-e2b7-4ba3-8849-a0dd4e4067a0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1a2f6b43-e2be-42fc-9844-6c181e41eb9f","path":"sprites/sSpace/sSpace.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1a2f6b43-e2be-42fc-9844-6c181e41eb9f","path":"sprites/sSpace/sSpace.yy",},"LayerId":{"name":"63678e3d-6fd8-426e-aa5b-82de766254e4","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sSpace","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"1a2f6b43-e2be-42fc-9844-6c181e41eb9f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1d82a3d5-ecc7-44e9-96d8-fa800d576559","path":"sprites/sSpace/sSpace.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1d82a3d5-ecc7-44e9-96d8-fa800d576559","path":"sprites/sSpace/sSpace.yy",},"LayerId":{"name":"63678e3d-6fd8-426e-aa5b-82de766254e4","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sSpace","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"1d82a3d5-ecc7-44e9-96d8-fa800d576559","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5031cd8b-bb03-4e56-a172-a17199990d80","path":"sprites/sSpace/sSpace.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5031cd8b-bb03-4e56-a172-a17199990d80","path":"sprites/sSpace/sSpace.yy",},"LayerId":{"name":"63678e3d-6fd8-426e-aa5b-82de766254e4","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sSpace","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"5031cd8b-bb03-4e56-a172-a17199990d80","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"07f6000d-3cc4-4848-b47b-56558fe706ea","path":"sprites/sSpace/sSpace.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"07f6000d-3cc4-4848-b47b-56558fe706ea","path":"sprites/sSpace/sSpace.yy",},"LayerId":{"name":"63678e3d-6fd8-426e-aa5b-82de766254e4","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sSpace","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"07f6000d-3cc4-4848-b47b-56558fe706ea","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b863759b-afd5-448e-bf21-a97528fe3143","path":"sprites/sSpace/sSpace.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b863759b-afd5-448e-bf21-a97528fe3143","path":"sprites/sSpace/sSpace.yy",},"LayerId":{"name":"63678e3d-6fd8-426e-aa5b-82de766254e4","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sSpace","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"b863759b-afd5-448e-bf21-a97528fe3143","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fd793976-b0a5-40cd-a0ec-0fb2456e734b","path":"sprites/sSpace/sSpace.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fd793976-b0a5-40cd-a0ec-0fb2456e734b","path":"sprites/sSpace/sSpace.yy",},"LayerId":{"name":"63678e3d-6fd8-426e-aa5b-82de766254e4","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sSpace","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"fd793976-b0a5-40cd-a0ec-0fb2456e734b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1daec469-9477-45d5-ab8f-ee9f642139d8","path":"sprites/sSpace/sSpace.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1daec469-9477-45d5-ab8f-ee9f642139d8","path":"sprites/sSpace/sSpace.yy",},"LayerId":{"name":"63678e3d-6fd8-426e-aa5b-82de766254e4","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sSpace","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"1daec469-9477-45d5-ab8f-ee9f642139d8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c5549944-ad4f-4808-9922-a704094a79eb","path":"sprites/sSpace/sSpace.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c5549944-ad4f-4808-9922-a704094a79eb","path":"sprites/sSpace/sSpace.yy",},"LayerId":{"name":"63678e3d-6fd8-426e-aa5b-82de766254e4","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sSpace","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"c5549944-ad4f-4808-9922-a704094a79eb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f9810e15-f1a0-4cc6-8428-c22e2676bb22","path":"sprites/sSpace/sSpace.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f9810e15-f1a0-4cc6-8428-c22e2676bb22","path":"sprites/sSpace/sSpace.yy",},"LayerId":{"name":"63678e3d-6fd8-426e-aa5b-82de766254e4","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sSpace","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","name":"f9810e15-f1a0-4cc6-8428-c22e2676bb22","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sSpace","path":"sprites/sSpace/sSpace.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 19.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"0c4f4df8-5f98-4b5a-88bf-b0cca4e2f1d6","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"25fec092-8b8f-46f3-b484-6f22b3560597","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c6dd5db8-b86c-4fbc-8531-df816131f5f2","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"69d82002-1185-4ece-9c02-27f8bc45cd0b","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fd880eea-8268-4c31-bb85-b62506d2b994","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cb886bea-7c4a-4c15-a6d3-cf720a43f3d8","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3d3a2d51-207e-441d-a020-b82bc57c8868","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"49ac27d7-1c56-4e64-9d96-132a7148beaa","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"17710103-c495-4cdd-8603-f87ef92c9861","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"86b9a167-e61d-4437-8651-d6ceab5de76f","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"33e7f927-e0fa-4cc8-8588-be5cc0e9c4f1","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"aa90f3d3-871f-4499-bd93-c0077bfe39d0","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8c7cde51-41c3-4479-a7a3-59b0cd191b27","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5f85bfb2-e15c-4003-83c1-4be01e304d92","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5eacd237-84bd-4857-b708-196c71234fb8","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fc132e80-2bec-4df4-9637-a53b65f66d37","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a9bb4b97-f6cf-4f7d-8014-5056a40154ad","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"470bd843-ca4c-4f06-8979-c9e397763328","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8073bfff-b549-4701-9f59-964190f6858b","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bc4c7caa-e2b7-4ba3-8849-a0dd4e4067a0","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dc7feb37-6f7a-4389-bbaf-22f42a680d05","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1a2f6b43-e2be-42fc-9844-6c181e41eb9f","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e564b9e2-0548-4083-92f5-844526a3b973","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1d82a3d5-ecc7-44e9-96d8-fa800d576559","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8512894f-5ce8-4c9b-9e66-a8c94f7dc199","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5031cd8b-bb03-4e56-a172-a17199990d80","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3b740be6-693a-4767-8d11-1fbbc0431656","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"07f6000d-3cc4-4848-b47b-56558fe706ea","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6553b92f-447d-463f-af76-be0db6d95fa9","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b863759b-afd5-448e-bf21-a97528fe3143","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"31bd399b-ad24-4420-8d0b-7fb35bfab18a","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fd793976-b0a5-40cd-a0ec-0fb2456e734b","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"52aa285a-fbd8-4c20-b3bc-1f095feebf72","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1daec469-9477-45d5-ab8f-ee9f642139d8","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"195a30ed-1633-44cc-8a05-fccbab11cf57","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c5549944-ad4f-4808-9922-a704094a79eb","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"83ee3f89-dec5-4e7e-8d19-21506c50faea","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f9810e15-f1a0-4cc6-8428-c22e2676bb22","path":"sprites/sSpace/sSpace.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sSpace","path":"sprites/sSpace/sSpace.yy",},
    "resourceVersion": "1.3",
    "name": "sSpace",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"63678e3d-6fd8-426e-aa5b-82de766254e4","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "sSpace",
  "tags": [],
  "resourceType": "GMSprite",
}